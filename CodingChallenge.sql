CREATE DATABASE CarRental;
USE CarRental;

CREATE TABLE Vehicles(
VehicleID INT PRIMARY KEY, 
Make VARCHAR(225),
Model vARCHAR(225),
Year INT,
DailyRate DECIMAL(18,2),
Status INT,
PassengerCapacity INT,
EngineCapacity INT);



CREATE TABLE Customers (
CustomerID INT PRIMARY KEY,
FirstName VARCHAR(50),
LastName VARCHAR(50),
Email VARCHAR(100),
PhoneNumber VARCHAR(20)
);

CREATE TABLE Lease (
LeaseID INT PRIMARY KEY,
VehicleID INT,
CustomerID INT,
StartDate DATE,
EndDate DATE,
LeaseType VARCHAR(20),
FOREIGN KEY (VehicleID) REFERENCES Vehicles(VehicleID),
FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID)
);


CREATE TABLE Payments (
PaymentID INT PRIMARY KEY,
LeaseID INT,
PaymentDate DATE,
Amount DECIMAL(10, 2),
FOREIGN KEY (LeaseID) REFERENCES Lease(LeaseID)
);

-- Insert data into Vehicle table
INSERT INTO Vehicles(VehicleID, Make, Model, Year, DailyRate , Status ,PassengerCapacity,EngineCapacity )
VALUES
(1, 'Toyota', 'Camry', 2022, 50.00, 1, 4, 1450),
(2, 'Honda', 'Civic', 2023, 45.00, 1, 7, 1500),
(3, 'Ford', 'Focus', 2022, 48.00, 0, 4, 1400),
(4, 'Nissan', 'Altima', 2023, 52.00, 1, 7, 1200),
(5, 'Chevrolet', 'Malibu', 2022, 47.00, 1, 4, 1800),
(6, 'Hyundai', 'Sonata', 2023, 49.00, 0, 7, 1400),
(7, 'BMW', '3 Series', 2023, 60.00, 1, 7, 2499),
(8, 'Mercedes', 'C-Class', 2022, 58.00, 1, 8, 2599),
(9, 'Audi', 'A4', 2022, 55.00, 0, 4, 2500),
(10, 'Lexus', 'ES', 2023, 54.00, 1, 4, 2500);

INSERT INTO Customers (CustomerID, FirstName, LastName, Email, PhoneNumber)
VALUES
(1, 'John', 'Doe', 'johndoe@example.com', '555-555-5555'),
(2, 'Jane', 'Smith', 'janesmith@example.com', '555-123-4567'),
(3, 'Robert', 'Johnson', 'robert@example.com', '555-789-1234'),
(4, 'Sarah', 'Brown', 'sarah@example.com', '555-456-7890'),
(5, 'David', 'Lee', 'david@example.com', '555-987-6543'),
(6, 'Laura', 'Hall', 'laura@example.com', '555-234-5678'),
(7, 'Michael', 'Davis', 'michael@example.com', '555-876-5432'),
(8, 'Emma', 'Wilson', 'emma@example.com', '555-432-1098'),
(9, 'William', 'Taylor', 'william@example.com', '555-321-6547'),
(10, 'Olivia', 'Adams', 'olivia@example.com', '555-765-4321');

INSERT INTO Lease(LeaseID, VehicleID, CustomerID, StartDate, EndDate, LeaseType)
VALUES
(1, 1, 1, '2023-01-01', '2023-01-05', 'Daily'),
(2, 2, 2, '2023-02-15', '2023-02-28', 'Monthly'),
(3, 3, 3, '2023-03-10', '2023-03-15', 'Daily'),
(4, 4, 4, '2023-04-20', '2023-04-30', 'Monthly'),
(5, 5, 5, '2023-05-05', '2023-05-10', 'Daily'),
(6, 4, 3, '2023-06-15', '2023-06-30', 'Monthly'),
(7, 7, 7, '2023-07-01', '2023-07-10', 'Daily'),
(8, 8, 8, '2023-08-12', '2023-08-15', 'Monthly'),
(9, 3, 3, '2023-09-07', '2023-09-10', 'Daily'),
(10, 10, 10, '2023-10-10', '2023-10-31', 'Monthly');


INSERT INTO Payments(PaymentID, LeaseID, PaymentDate, Amount)
VALUES
(1, 1, '2023-01-03', 200.00),
(2, 2, '2023-02-20', 1000.00),
(3, 3, '2023-03-12', 75.00),
(4, 4, '2023-04-25', 900.00),
(5, 5, '2023-05-07', 60.00),
(6, 6, '2023-06-18', 1200.00),
(7, 7, '2023-07-03', 40.00),
(8, 8, '2023-08-14', 1100.00),
(9, 9, '2023-09-09', 80.00),
(10, 10, '2023-10-25', 1500.00);

 ---Update the daily rate for a Mercedes car to 68.

UPDATE Vehicles
SET DailyRate = 68.00
WHERE Make = 'Mercedes';




DELETE FROM Payments
WHERE LeaseID IN (SELECT leaseID FROM Lease WHERE customerID = 10);

DELETE FROM Lease
WHERE customerID = 10;

DELETE FROM Customers
WHERE CustomerID = 10;


--- Rename the "paymentDate" column in the Payment table to "transactionDate"

EXEC sp_rename 'Payments.PaymentDate', 'TransactionDate', 'COLUMN';

---Find a specific customer by email.

SELECT *FROM Customers
WHERE email = 'sarah@example.com';


---Get active leases for a specific customer.

SELECT  Make, Model,Year
FROM Lease L
JOIN Vehicles V ON L.VehicleID = V.VehicleID
WHERE L.CustomerID = 1  
AND V.Status = 1;


---Find all payments made by a customer with a specific phone number

SELECT Amount
FROM Payments p
JOIN Lease l ON p.leaseID = l.leaseID
JOIN Customers c ON l.customerID = c.customerID
WHERE c.PhoneNumber = '555-789-1234';

---. Calculate the average daily rate of all available cars.

SELECT AVG(DailyRate) AS AverageDailyRate
FROM Vehicles
WHERE status = 1;


----Find the car with the highest daily rate.SELECT TOP 1 * FROM Vehicles order by DailyRate desc;----Retrieve all cars leased by a specific customer.SELECT V.*, L.*
FROM Vehicles v
JOIN Lease L ON V.vehicleID = L.vehicleID
WHERE L.CustomerID = 3;---Find the details of the most recent lease.SELECT TOP 1 * FROM Lease ORDER BY EndDate DESC;---. List all payments made in the year 2023.SELECT *
FROM Payments
WHERE YEAR(TransactionDate) = 2023;


----Retrieve customers who have not made any payments

SELECT C.*
FROM Customers C
LEFT JOIN Lease L ON C.CustomerID = L.CustomerID
LEFT JOIN Payments P ON L.LeaseID = P.LeaseID
WHERE P.PaymentID IS NULL;

--. Retrieve Car Details and Their Total Payments.SELECT V.*, SUM(P.Amount) AS TotalPayments
FROM Vehicles V
JOIN Lease L ON V.VehicleID = L.VehicleID
LEFT JOIN Payments P ON L.LeaseID = P.LeaseID
GROUP BY V.VehicleID, Make, Model, Year, DailyRate,Status, PassengerCapacity,EngineCapacity;



-----. Calculate Total Payments for Each Customer.

SELECT C.*,SUM(P.amount) AS TotalPayments FROM Customers C
LEFT JOIN Lease L ON C.CustomerID = L.CustomerID
LEFT JOIN Payments P ON L.LeaseID = P.LeaseID
GROUP BY C.CustomerID, FirstName, LastName, Email, PhoneNumber;


---List Car Details for Each Lease.SELECT LeaseID, Make, Model,Year DailyRate,Status,PassengerCapacity,EngineCapacity
FROM Lease L
JOIN Vehicles V ON L.VehicleID = V.VehicleID;

--Retrieve Details of Active Leases with Customer and Car Information
SELECT L.*, C.firstName, C.lastName, V.make, V.model, V.year FROM Lease L
JOIN Customers C ON L.customerID = C.customerID
JOIN Vehicles V ON L.vehicleID = V.vehicleID WHERE L.endDate > GETDATE();



----Find the Customer Who Has Spent the Most on Leases.
SELECT TOP 1 C.customerID, C.firstName, C.lastName, SUM(P.amount) as totalPayments FROM Customers C
JOIN Lease L ON C.customerID = L.customerID
JOIN Payments P ON L.leaseID = P.leaseID GROUP BY C.customerID, C.firstName, C.lastName ORDER BY totalPayments DESC;



---To list all cars with their current lease information, you can use the following SQL query:

SELECT V.*, L.startDate, L.endDate FROM Vehicles V JOIN Lease L ON V.vehicleID = L.vehicleID;